# FoundryVTT Mobile Token Movement

Use your mobile device to control your token with a minimal interface.  
Useful for in-person games with a player-facing monitor using the module Monk's Common Display.  

## How To Use
To enable the mobile interface, accept the popup dialog with your mobile device.  
Any device with a width smaller than 600px will get this popup dialog when connecting.

Make sure you have a character assigned to the users that will be on a mobile device.

## Compatibility
Tested with these modules:
- Monk's Common Display
- Auto-rotate

## Screenshots
#### The mobile interface
![](assets/screenshot1.png)
